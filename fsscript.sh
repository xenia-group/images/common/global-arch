#!/bin/bash
dracut --force --no-hostonly --kver $(ls /lib/modules/)
cp /usr/lib/modules/*/vmlinuz /boot/
chown root:root /etc/sudoers

# rm -rf /opt
# mkdir /usr/opt
# ln -sf /usr/opt /

cp /etc/passwd /.recovery/etc/passwd
cp /etc/shadow /.recovery/etc/shadow

echo "recovery:x:1000:1000::/home/recovery:/bin/bash" >> /.recovery/etc/passwd
echo "recovery:$6$ovJXS/P4rKaURNaD$IUmaP2JW5uiJgrFVr31bEMb6kEF.ARL.x23m.qvyJ3.oRRbJ1qQ/pU5R2VocEzunYqSGF/YvLFGqF5gn0BQY90:19574::::::" >> /.recovery/etc/shadow

sed s/wheel:x:10:root/wheel:x:10:root,recovery/ /etc/group > /.recovery/etc/group
echo "recovery:x:1000:" >> /.recovery/etc/group

chown 1000:1000 -R /.recovery/home/recovery

cp /usr/share/i18n/SUPPORTED /etc/locale.gen
locale-gen

systemctl enable bluetooth
systemctl enable NetworkManager
systemctl enable cups
systemctl enable systemd-timesyncd
systemctl enable lvm2-monitor
systemctl enable qemu-guest-agent
systemctl enable spice-vdagentd

systemctl --global enable pipewire.socket pipewire-pulse.socket wireplumber.service

rm /boot/*.old

cp /boot/initramfs* /boot/initramfs.img
